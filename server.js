var express = require("express");
var app = express();
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var url = 'mongodb://localhost:27017/test2';

app.get('/',function(req,res){
    res.send("Hello world");
});

app.get('/signin',function(req,res){
    res.send("<h1>Hello world</h1>");
});

app.get('/signout',function(req,res){
    res.send("<h1>Signed out</h1>");
});

MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server.");
  db.close();
});

var portId = 8000;
var server = app.listen(portId,function(){
    console.log("Server started at "+portId)
});